package br.com.itau.campanha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CampanhaAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(CampanhaAppApplication.class, args);
    }

}
